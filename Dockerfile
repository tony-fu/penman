FROM python:3.6.6-stretch
WORKDIR /root
COPY main.py .
COPY requirements.txt .
RUN pip install --upgrade pip
RUN pip install pyinstaller
RUN pip install -r requirements.txt
CMD ["bash", "-c", "pyinstaller --onefile main.py"]
