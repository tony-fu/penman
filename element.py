class Element:
    def __init__(self, *children) -> None:
        self.attributes = {}
        self.children = children
        self.tag = 'placeholder'
        self.classes = set()

    def with_class(self, attr_class) -> 'Element':
        self.classes.add(attr_class)
        self.attributes.update({'class': ' '.join(self.classes)})
        return self

    def with_id(self, attr_id) -> 'Element':
        self.attributes.update({'id': attr_id})
        return self

    @property
    def html(self) -> str:
        attributes = ' ' + ' '.join([f'{k}="{v}"' for k, v in self.attributes.items()])
        if len(self.children) == 1 and isinstance(next(iter(self.children)), str):
            children = next(iter(self.children))
        else:
            children = ''.join(child.html for child in self.children)
        return f'<{self.tag}{attributes}>{children}</{self.tag}>'

    def __str__(self) -> str:
        return self.html


class A(Element):
    def __init__(self, *children) -> None:
        super().__init__(*children)
        self.tag = 'a'

    def with_href(self, attr_href) -> Element:
        self.attributes.update({'href': attr_href})
        return self


class P(Element):
    def __init__(self, *children) -> None:
        super().__init__(*children)
        self.tag = 'p'


class Section(Element):
    def __init__(self, *children) -> None:
        super().__init__(*children)
        self.tag = 'section'


class Span(Element):
    def __init__(self, *children) -> None:
        super().__init__(*children)
        self.tag = 'span'


class Main(Element):
    def __init__(self, *children) -> None:
        super().__init__(*children)
        self.tag = 'main'


class Time(Element):
    def __init__(self, *children) -> None:
        super().__init__(*children)
        self.tag = 'time'


class Html(Element):
    def __init__(self, *children) -> None:
        super().__init__(*children)

        self.tag = 'html'

    def with_lang(self, attr_lang) -> Element:
        self.attributes.update({'lang': attr_lang})
        return self


class Title(Element):
    def __init__(self, *children) -> None:
        super().__init__(*children)
        self.tag = 'title'


class Head(Element):
    def __init__(self, *children) -> None:
        super().__init__(*children)
        self.tag = 'head'


class Body(Element):
    def __init__(self, *children) -> None:
        super().__init__(*children)
        self.tag = 'body'


class Header(Element):
    def __init__(self, *children) -> None:
        super().__init__(*children)
        self.tag = 'header'


class Heading1(Element):
    def __init__(self, *children) -> None:
        super().__init__(*children)
        self.tag = 'h1'


class Heading2(Element):
    def __init__(self, *children) -> None:
        super().__init__(*children)
        self.tag = 'h2'


class Heading3(Element):
    def __init__(self, *children) -> None:
        super().__init__(*children)
        self.tag = 'h3'


class Text(Element):
    def __init__(self, *children) -> None:
        super().__init__(*children)
        self.tag = None

    @property
    def html(self) -> str:
        return next(iter(self.children))


class Style(Element):
    def __init__(self, *children) -> None:
        super().__init__(*children)
        self.tag = 'style'


class Stylesheet(Element):
    def __init__(self, *children) -> None:
        super().__init__(*children)
        self.tag = 'link'
        self.attributes = {'rel': 'stylesheet', 'href': next(iter(children))}

    @property
    def html(self):
        attributes = ' ' + ' '.join([f'{k}="{v}"' for k, v in self.attributes.items()])
        return f'<{self.tag}{attributes}>'
