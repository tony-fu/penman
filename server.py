#!/usr/bin/env python3
from http.server import HTTPServer, BaseHTTPRequestHandler
from pathlib import Path


class StaticServer(BaseHTTPRequestHandler):

    def do_GET(self):
        file_path = Path('public' + self.path)
        if file_path.is_file():
            self.send_response(200)
            if file_path.suffix == 'css':
                self.send_header('Content-type', 'text/css')
            elif file_path.suffix == 'json':
                self.send_header('Content-type', 'application/json')
            elif file_path.suffix == 'js':
                self.send_header('Content-type', 'application/javascript')
            elif file_path.suffix == 'ico':
                self.send_header('Content-type', 'image/x-icon')
            elif file_path.suffix == 'html':
                self.send_header('Content-type', 'text/html')
            self.end_headers()
            with file_path.open('rb') as f:
                self.wfile.write(f.read())
        elif file_path.with_suffix('.html').exists():
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            with file_path.with_suffix('.html').open('rb') as f:
                self.wfile.write(f.read())
        elif (file_path / 'index.html').exists():
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            with (file_path / 'index.html').open('rb') as f:
                self.wfile.write(f.read())
        else:
            self.send_response(404)


def run(server_class=HTTPServer, handler_class=StaticServer, port=8000):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print('Starting http server on port {}'.format(port))
    httpd.serve_forever()


run()
